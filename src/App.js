import './App.css';

// import { Fragment } from 'react';
// importing appNavbar function from the aooNavBar.js
import AppNavbar from './components/AppNavBar';

// import { Container } from 'react-bootstrap';
import Register from './pages/Register';

// importing banner
// import Banner from './Banner';

// import Highlights from './Highlights';

import Home from './pages/Home';
import Courses from './pages/Courses';
import Login from './pages/Login';
import Logout from './pages/Logout';
import NotFound from './pages/NotFound';
import CourseView from './components/CourseView';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext';

function App() {
  const [user, setUser] = useState(null)

  useEffect(() => {
    console.log(user);
  }, [user])

  const unSetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {

    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(result => result.json())
      .then(data => {
        console.log(data);

        if (localStorage.getItem('token') !== null) {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
        }else{
          setUser(null)
        }

      })

  }, [])

  // Storing info in a context object is done by providing the info using the corresponding "Provider" and passing info thru the prop value
  // all info/data provided to the Provider component can be access later on from the context object properties

  return (
    <UserProvider value={{ user, setUser, unSetUser }}>
      <Router>
        <AppNavbar />
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/courses' element={<Courses />} />
          <Route path='/login' element={<Login />} />
          <Route path='/register' element={<Register />} />
          <Route path='/logout' element={<Logout />} />
          <Route path='*' element={<NotFound />} />
          <Route path='/course/:courseId' element={<CourseView />} />
        </Routes>
      </Router>
    </UserProvider>
  );
}

export default App;
