import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { Row, Col } from 'react-bootstrap'
import { useEffect, useState, useContext } from 'react';
import React from 'react';
import UserContext from '../UserContext'
import { Link } from "react-router-dom";

export default function CourseCard({ courseProp }) {
    // console.log(course.name);
    // const name = courseProp.name
    // const id = courseProp.id
    // const description = courseProp.description
    // const price = courseProp.price
    const {_id, name, description, price } = courseProp
    // The course in the "course " in the CourseCard component is called a "prop" which is a shorthand for property

    // The curly braces are used for props to signify

    // use the state hook for this component to be able to store its state
    // states are used to keep track of info related to individual components
    // const [getter, setter] = useState(initialGetterValue)

    const [enrollees, setEnrollees] = useState(0);
    // Initial Value of enrollees
    // console.log(enrollees);

    // if u want to reassign the value of the state
    // setEnrollees(1)

    // console.log(enrollees);

    const { user } = useContext(UserContext)
    function enroll() {
        if (enrollees < 30) {
            setEnrollees(enrollees + 1)
        }
    }

    // Activity
    const [seats, setSeats] = useState(30);
    function AvailableSeats() {
        if (seats <= 0) {
            return alert("no more seats")

        }
        if (seats <= 30) {
            setSeats(seats - 1)
        }
    }

    // for disabling button once seats reached 0
    const [isDisabled, setIsDisabled] = useState(false)
    useEffect(() => {
        if (seats === 0) {
            alert("congratulations")
            setIsDisabled(true)
        }
    }, [seats])

    // Define a 'useEffect' hook to have the "Coursecard" component do perform a certain task
    // This will run automatically.
    // Syntax:
    // useEffect(sideEffect/functyion,[dependencies])
    // sideEffect/function - it will run on the first load and will reload depending on the dependency array

    return (
        <Row className='mt-3'>
            <Col>
                <Card>
                    <Card.Body>
                        <Card.Title>{name}</Card.Title>
                        <Card.Subtitle>Description:</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price:</Card.Subtitle>
                        <Card.Text>{price}</Card.Text>
                        <Card.Subtitle>Enrolees:</Card.Subtitle>
                        <Card.Text>{enrollees}</Card.Text>
                        <Card.Subtitle>Available Seats:</Card.Subtitle>
                        <Card.Text>{seats}</Card.Text>
                        {
                            user ?
                                <Button as = {Link} to = {`/course/${_id}`} disabled={isDisabled}>See more details</Button>

                            :

                            <Button as = {Link} to = "/login">Login</Button>
                        }
                        
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
}