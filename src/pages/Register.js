import { Button, Form } from 'react-bootstrap'
import { Fragment, useState } from 'react'
import { Navigate, useNavigate } from "react-router-dom"
import { useContext } from "react";
import UserContext from "../UserContext";
import Swal from 'sweetalert2';


// import the hooks needed in our page
import { useEffect } from 'react'
export default function Register() {

    // Create 3 new states to store value from input of the email, password and confirm password
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [mobileNo, setMobileNo] = useState('')
    // const[user, setUser] = useState(localStorage.getItem("email"))

    const { user, setUser } = useContext(UserContext)
    const navigate = useNavigate()


    // state for button 

    const [isActive, setIsActive] = useState(false);

    useEffect(() => {
        if (email !== '' && password !== '') {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password])


    // useEffect(()=>{
    //     console.log(email);
    //     console.log(password);
    //     console.log(confirmPassword);
    // }, [email,password,confirmPassword])

    function register(event) {
        event.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                password: password,
                mobileNo: mobileNo
            })
        })
            .then(result => result.json())
            .then(data => {
                console.log(data);
                if(data === false){
                    Swal.fire({
                        title: "Register Failed",
                        icon: "error",
                        text: "Please try again"
                    })
                }else{
    
                    Swal.fire({
                        title: "Register Successful!",
                        icon: "success",
                        text: "Welcome to Zuitt"
                    })
    
                    navigate("/login")
    
                }
            })


        // localStorage.setItem("email", email);
        // setUser(localStorage.getItem("email"))
        // alert('Congratulations, you are now registered!')

        // setEmail('');
        // setPassword('');
        // setConfirmPassword('');

        // navigate("/")

    }

    return (
        user ?
            <Navigate to="*" />
            :
            <Fragment>
                <h1 className='text-center mt-5'>Register</h1>
                <Form className='mt-5' onSubmit={event => register(event)}>
                    <Form.Group className="mb-3" controlId="formBasicFirstName">
                        <Form.Label>First Name</Form.Label>
                        <Form.Control
                            type="string"
                            placeholder="Enter First Name"
                            value={firstName}
                            onChange={event => setFirstName(event.target.value)}
                            required
                        />
                        <Form.Text className="text-muted">
                            We'll never share your information with anyone else.
                        </Form.Text>
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicLastName">
                        <Form.Label>Last Name</Form.Label>
                        <Form.Control
                            type="string"
                            placeholder="Enter Last Name"
                            value={lastName}
                            onChange={event => setLastName(event.target.value)}
                            required
                        />
                        <Form.Text className="text-muted">
                            We'll never share your information with anyone else.
                        </Form.Text>
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control
                            type="email"
                            placeholder="Enter email"
                            value={email}
                            onChange={event => setEmail(event.target.value)}
                            required
                        />
                        <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                        </Form.Text>
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                            type="password"
                            placeholder="Password"
                            value={password}
                            onChange={event => setPassword(event.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicMobileNo">
                        <Form.Label>Mobile No</Form.Label>
                        <Form.Control
                            type="number"
                            placeholder="Enter Mobile No"
                            value={mobileNo}
                            onChange={event => setMobileNo(event.target.value)}
                            required
                        />
                        <Form.Text className="text-muted">
                            We'll never share your information with anyone else.
                        </Form.Text>
                    </Form.Group>

                    {/* <Form.Group className="mb-3" controlId="formBasicConfirmPassword">
                    <Form.Label>Confirm Password</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Confirm your Password"
                        value={confirmPassword}
                        onChange={event => setConfirmPassword(event.target.value)}
                        required
                    />
                </Form.Group> */}

                    {/* in this code block, we do conditional rendering depending on the state of our isActive */}
                    {
                        isActive ?
                            <Button variant="primary" type="submit" >
                                Submit
                            </Button>
                            :
                            <Button variant="danger" type="submit" disabled>
                                Submit
                            </Button>
                    }




                </Form>
            </Fragment>

    )
}